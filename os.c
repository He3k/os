#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>

// Datatypes
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long size_t;

typedef int bool;

#define true 1
#define false 0
#define null 0


#define attribute(value) __attribute__((value))

// Sizes
#define MMU_PAGE_TABLE_ENTRIES_COUNT 1024
#define MMU_KERNEL_PAGES_COUNT 768


#define malloc_a(size, align) kmalloc_a(size, align)


struct page_directory_entry_t {
  u8 present : 1;
  u8 read_write : 1;
  u8 user_supervisor : 1;
  u8 write_through : 1;
  u8 cache_disabled : 1;
  u8 accessed : 1;
  u8 zero : 1;
  u8 page_size : 1;
  u8 ignored : 1;
  u8 available : 3;
  u32 page_table_addr : 20;
} attribute(packed);


struct page_table_entry_t {
  u8 present : 1;
  u8 read_write : 1;
  u8 user_supervisor : 1;
  u8 write_through : 1;
  u8 cache_disabled : 1;
  u8 accessed : 1;
  u8 dirty : 1;
  u8 zero : 1;
  u8 global : 1;
  u8 available : 3;
  u32 page_phys_addr : 20;
} attribute(packed);


static struct page_directory_entry_t kpage_directory attribute(aligned(4096));
static struct page_table_entry_t kpage_table[MMU_PAGE_TABLE_ENTRIES_COUNT] attribute(aligned(4096));

// init pages 4kb
void mmu_init()
{
  // clean 0
  memset(&kpage_directory, 0, sizeof(struct page_directory_entry_t));
  
  // catalog 
  kpage_directory.accessed = 0;
  kpage_directory.cache_disabled = 0;
  kpage_directory.ignored = 0;
  kpage_directory.page_size = 0; /* 4KB */
  kpage_directory.present = 1; 
  kpage_directory.read_write = 1; 
  kpage_directory.user_supervisor = 1; 
  kpage_directory.write_through = 1;
  kpage_directory.page_table_addr = (u32)kpage_table >> 12; 
  
  // table pages
  for (int i = 0; i < MMU_PAGE_TABLE_ENTRIES_COUNT; ++i) {
    kpage_table[i].accessed = 0;
    kpage_table[i].cache_disabled = 0;
    kpage_table[i].dirty = 0;
    kpage_table[i].global = 1;
    kpage_table[i].present = 1; 
    kpage_table[i].read_write = 1; 
    kpage_table[i].user_supervisor = 1; 
    kpage_table[i].write_through = 1;
    kpage_table[i].page_phys_addr = (i * 4096) >> 12; /* assume 4Kb pages */
  }
}

// get catalog pages
extern struct page_directory_entry_t* mmu_get_kdirectory()
{
  return &kpage_directory;
}

// get table pages
extern struct page_table_entry_t* mmu_get_ktable()
{
  return kpage_table;
}

// print
void mmu_print()
{
  printf("present: %d ", kpage_directory.present);
  printf("read_write: %d ", kpage_directory.read_write);
  printf("user_supervisor: %d ", kpage_directory.user_supervisor);
  printf("write_through: %d ", kpage_directory.write_through);
  printf("cache_disabled: %d ", kpage_directory.cache_disabled);
  printf("accessed: %d ", kpage_directory.accessed);
  printf("page_size: %d ", kpage_directory.page_size);
  printf("ignored: %d ", kpage_directory.ignored);
  printf("page_table_addr: 0x%x ", kpage_directory.page_table_addr);
  printf("page_table_phys_addr: 0x%x\n\n\n", &kpage_directory);
  
  // print table
  for (int i = 0; i < MMU_PAGE_TABLE_ENTRIES_COUNT; ++i) {
    printf("accessed: %d\t", kpage_table[i].accessed);
    printf("cache_disabled: %d\t", kpage_table[i].cache_disabled);
    printf("dirty: %d\t", kpage_table[i].dirty);
    printf("global: %d\t", kpage_table[i].global);
    printf("present: %d\t", kpage_table[i].present);
    printf("read_write: %d\t", kpage_table[i].read_write);
    printf("user_supervisor: %d\t", kpage_table[i].user_supervisor);
    printf("write_through: %d\t", kpage_table[i].write_through);
    printf("page_virt_addr: 0x%x\t", kpage_table[i].page_phys_addr);
    printf("page_phys_addr: 0x%x\t", &kpage_table[i]);
    printf("\n");
  }
}

int main(int argc, const char *argv[]) {

  
  mmu_init();
  mmu_print();
  
  printf("\n");
  
  return 0;
}
