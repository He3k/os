#include <stdio.h>

int main() {
    int count = 0;
    int fac = 1;
    printf("Enter count: ");
    scanf("%d", &count);
    for (int i = 0; i < count; i++) {
        fac += fac * i;
        //printf("%d\n", fac);
    }
    printf("Factorial %d = %d\n", count, fac);
    return 0;
}